import { createRouter, createWebHistory, RouteRecordRaw } from 'vue-router';

const routes: Array<RouteRecordRaw> = [
  {
    path: '',
    name: 'courses',
    component: () => import(/* webpackChunkName: "about" */ '../views/CoursesView.vue'),
  },
  {
    path: '/:id',
    name: 'course',
    component: () => import(/* webpackChunkName: "about" */ '../views/CourseView.vue'),
  },
];

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes,
});

export default router;
