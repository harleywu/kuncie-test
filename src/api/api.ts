import axiosInstance from './axios';

export function getCourses(): Promise<any> {
  return axiosInstance.get('/available-classes');
}

export function getCourse(id: number): Promise<any> {
  return axiosInstance.get(`/learning-class?id=${id}`);
}

//
export function registerCourse(id: number, name: string, email: string): Promise<any> {
  const urlencoded = new URLSearchParams();
  urlencoded.append('classId', id.toString());
  urlencoded.append('attendeeFullName', name);
  urlencoded.append('attendeeEmail', email);

  return axiosInstance.post('/join-class', urlencoded, {
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded',
    },
  });
}
