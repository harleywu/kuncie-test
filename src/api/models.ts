interface Wrapper {
  id: number,
  name: string
}

export type Course = Wrapper

export interface Mentor extends Wrapper {
  description: string;
}

export interface WrapperMentors extends Mentor {
  mentors: Mentor[];
}

export interface ResponseErr {
  code: number,
  message: string,
}
