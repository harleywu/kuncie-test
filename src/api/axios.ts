import axios from 'axios';

const axiosInstance = axios.create({
  baseURL: 'https://baee0c5d-15e7-45e2-ae1b-392ba74406a9.mock.pstmn.io',
  timeout: 100000,
  headers: {
    'Content-Type': 'application/json',
  },
});

export default axiosInstance;
