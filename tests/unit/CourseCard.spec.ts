import { shallowMount } from '@vue/test-utils';
import CourseCard from '@/components/CourseCard.vue';

describe('CourseCard.vue', () => {
  it('renders props.msg when passed', () => {
    const title = 'testing title';
    const description = 'testing description';

    const wrapper = shallowMount(CourseCard, {
      props: {
        title,
        description,
      },
    });
    expect(wrapper.props().title).toMatch(title);
    expect(wrapper.props().description).toMatch(description);
  });
});
